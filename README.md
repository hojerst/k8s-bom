# Extract images from kubernetes manifests

Simple [yq] based tool to extract images from a stream of Kubernetes manifests. You can
use this tool to compile a list of images for a bill of materials. You can use the 
bill of materials to preload images in disconnected setups.

## Prerequisites

* Installed yq version 4.x

## Example usage

### Extract all USED images of a running Kubernetes cluster

```bash
❯ kubectl get pod,deployment,replicaset,daemonset,statefulset,replicationcontroller,cronjob,job -A -o yaml | yq eval '.items[] | splitDoc' - | ./extract-images.sh
- rancher/local-path-provisioner:v0.0.19
- rancher/metrics-server:v0.3.6
- rancher/coredns-coredns:1.8.3
- rancher/library-traefik:1.7.19
- rancher/klipper-lb:v0.2.0
- rancher/klipper-helm:v0.6.6-build20211022
```

### Extract all USED images from a helm chart

```bash
❯ helm template grafana/loki-stack --set prometheus.enabled=true | ./extract-images.sh
- "prom/node-exporter:v1.0.1"
- "grafana/promtail:2.1.0"
- "quay.io/coreos/kube-state-metrics:v1.9.7"
- "prom/alertmanager:v0.21.0"
- "jimmidyson/configmap-reload:v0.4.0"
- "prom/pushgateway:v1.2.0"
- "prom/prometheus:v2.21.0"
- bats/bats:v1.1.0

❯ helm template grafana/loki-stack | ./extract-images.sh

- "grafana/promtail:2.1.0"
- bats/bats:v1.1.0
```

[yq]: https://mikefarah.gitbook.io/yq/
