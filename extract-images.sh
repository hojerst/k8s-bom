#!/bin/sh

yq eval-all '
. as $item ireduce ([]; . +
  [
    $item | (
      select(
        (.apiVersion == "apps/v1" and .kind | match("Deployment|ReplicaSet|DaemonSet"))
        or (.apiVersion == "v1" and .kind | match("StatefulSet|ReplicationController"))
      ) | (
        .spec.template.spec.containers[].image,
        .spec.template.spec.initContainers[].image
      ),
      select(
        .apiVersion == "batch/v1" and .kind | match("Job|CronJob")
      ) | (
        .spec.template.spec.containers[].image
      ),
      select(.apiVersion == "v1" and .kind == "Pod") | (
        .spec.containers[].image,
        .spec.initContainers[].image
      )
    )
  ]
)
| unique
| sort
| .[]
' "$@"
